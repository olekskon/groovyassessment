/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 ***************************************************************/
/**
 *
 * User: Oleksandr Kononov
 * Date: 2019-08-12
 */

package test

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import view.ConsoleView
import view.Menu

import static org.junit.jupiter.api.Assertions.*

class ConsoleViewTest {

  final ByteArrayOutputStream outContent = new ByteArrayOutputStream()
  final PrintStream originalOut = System.out

  ConsoleView view

  @BeforeEach
  void setUp(){
    System.setOut(new PrintStream(outContent))
    view = new ConsoleView()
  }

  @AfterEach
  void tearDown(){
    System.setOut(originalOut)
  }

  @Test
  void testDisplay() {
    // Display Main Menu
    String expectedMenuString = "\nMAIN MENU\n\n" +
      ConsoleView.ADD+". Add new user\n" +
      ConsoleView.REMOVE+". Remove existing user\n" +
      ConsoleView.EDIT+". Edit existing user\n" +
      ConsoleView.LIST_USER+". List specific user\n" +
      ConsoleView.LIST_USERS+". List all users\n" +
      ConsoleView.QUIT+". Quit\n"+
      "> "
    view.display()
    assertEquals(expectedMenuString, outContent.toString())
  }

  @Test
  void testShow() {
    String testString = "Testing"
    view.show(testString)
    assertEquals(testString+"\n", outContent.toString())

    view.show("")
    assertEquals(testString+"\n", outContent.toString())

    view.show(null)
    assertEquals(testString+"\n", outContent.toString())
  }

  @Test
  void testAddMenuOperation() {
    String expectedMenuString = "\nMAIN MENU\n\n" +
      ConsoleView.ADD+". Add new user\n" +
      ConsoleView.REMOVE+". Remove existing user\n" +
      ConsoleView.EDIT+". Edit existing user\n" +
      ConsoleView.LIST_USER+". List specific user\n" +
      ConsoleView.LIST_USERS+". List all users\n" +
      ConsoleView.QUIT+". Quit\n"+
      "> "
    String testListOperation = "list menu operation"

    // No List Menu operations
    view.display()
    assertEquals(expectedMenuString, outContent.toString())

    // Add list menu operations - to print testListOperations
    view.addMenuOperation(Menu.MAIN) {print testListOperation}
    view.display()
    assertEquals(expectedMenuString+expectedMenuString+testListOperation, outContent.toString())

  }

  @Test
  void testChangeGetMenu(){
    assertEquals(Menu.MAIN, view.getCurrentMenu())
    view.changeMenu(Menu.ADD)
    assertEquals(Menu.ADD, view.getCurrentMenu())
  }
}
