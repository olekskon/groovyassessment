/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 ***************************************************************/
/**
 *
 * User: Oleksandr Kononov
 * Date: 2019-08-12
 */

package test

import model.Person
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

import static org.junit.jupiter.api.Assertions.*

class PersonTest {

  Person p

  @BeforeEach
  void setUp(){
    p = new Person("Test", "Tester", "353812345678", "tester@test.test", "none")
  }

  @Test
  void testConstructor(){
    // Verify constructor creates person object with correct information
    Person person = new Person("T1", "T2", "1234567890", "mail@mail.com", "none")
    assertEquals("T1", person.getFirstName())
    assertEquals("T2", person.getLastName())
    assertEquals("1234567890", person.getPhone())
    assertEquals("mail@mail.com", person.getEmail())
    assertEquals("none", person.getAddress())
    assertEquals("", person.nullReport())

    Person person2 = new Person(null, null, null, null, null)
    assertNull(person2.getFirstName())
    assertNull(person2.getLastName())
    assertNull(person2.getPhone())
    assertNull(person2.getEmail())
    assertNull(person2.getAddress())

    // Verify null report gives us correct report on null fields
    assertEquals("phone is not set!\naddress is not set!\n" +
      "firstName is not set!\nlastName is not set!\nemail is not set!\n", person2.nullReport())
  }

  @Test
  void testSetFirstName() {
    assertEquals("Test", p.firstName)
    p.firstName = "NotTest"
    assertEquals("NotTest", p.firstName)

    // Verify we can't change first name to null
    p.firstName = null
    assertEquals("NotTest", p.firstName)

    // Verify we can't change first name to empty string
    p.firstName = ""
    assertEquals("NotTest", p.firstName)

    // Verify we can't change first name to blank spaces
    p.firstName = "    "
    assertEquals("NotTest", p.firstName)
  }

  @Test
  void testSetLastName() {
    assertEquals("Tester", p.lastName)
    p.lastName = "NotTester"
    assertEquals("NotTester", p.lastName)

    // Verify we can't change last name to null
    p.lastName = null
    assertEquals("NotTester", p.lastName)

    // Verify we can't change last name to empty string
    p.lastName = ""
    assertEquals("NotTester", p.lastName)

    // Verify we can't change last name to blank spaces
    p.lastName = "       "
    assertEquals("NotTester", p.lastName)
  }

  @Test
  void testSetPhone() {
    assertEquals("353812345678", p.phone)
    p.phone = "1234567890"
    assertEquals("1234567890", p.phone)

    // Verify we can't set phone number to null
    p.phone = null
    assertEquals("1234567890", p.phone)

    // Verify we can't set phone number to empty string
    p.phone = ""
    assertEquals("1234567890", p.phone)

    // Verify we can't set phone number to blank spaces
    p.phone = " "
    assertEquals("1234567890", p.phone)

    // Verify we phone number length requirement
    p.phone = "1234"
    assertEquals("1234567890", p.phone)

    // Verify we can't have non digits in phone number field
    p.phone = "abcdefg123456"
    assertEquals("1234567890", p.phone)
  }

  @Test
  void testSetEmail() {
    assertEquals("tester@test.test", p.email)
    p.email = "mail@mail.com"
    assertEquals("mail@mail.com", p.email)

    // Verify we can't set email to null
    p.email = null
    assertEquals("mail@mail.com", p.email)

    // Verify we can't set email to empty string
    p.email = ""
    assertEquals("mail@mail.com", p.email)

    // Verify we can't set email to blank spaces
    p.email = " "
    assertEquals("mail@mail.com", p.email)

    // Verify email format '@'
    p.email = "testtester.test"
    assertEquals("mail@mail.com", p.email)

    // Verify email format '.'
    p.email = "testtester@test"
    assertEquals("mail@mail.com", p.email)

    // Verify email format '@'
    p.email = "testtester.test"
    assertEquals("mail@mail.com", p.email)

    // Verify email format 'no user'
    p.email = "@test.com"
    assertEquals("mail@mail.com", p.email)

    // Verify email format 'no full stops in domain'
    p.email = "testtester@.com.com"
    assertEquals("mail@mail.com", p.email)
  }

  @Test
  void testSetAddress() {
    assertEquals("none", p.address)
    p.address = "8 nowhere, nowhere"
    assertEquals("8 nowhere, nowhere", p.address)

    // Verify we can't change address to null
    p.address = null
    assertEquals("8 nowhere, nowhere", p.address)

    // Verify we can't change address to empty string
    p.address = ""
    assertEquals("8 nowhere, nowhere", p.address)

    // Verify we can't change address to blank spaces
    p.address = " "
    assertEquals("8 nowhere, nowhere", p.address)
  }
}
