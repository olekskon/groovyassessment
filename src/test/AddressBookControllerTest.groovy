/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 ***************************************************************/
/**
 *
 * User: Oleksandr Kononov
 * Date: 2019-08-12
 */

package test

import controller.AddressBookController
import model.Person
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

import static org.junit.jupiter.api.Assertions.*

class AddressBookControllerTest {

  AddressBookController controller

  @BeforeEach
  void setUp(){
    controller = new AddressBookController();
    controller.addPerson("Test1", "Tester1", "1234567890", "test1@test.com", "na1");
    controller.addPerson("Test2", "Tester2", "0987654321", "test2@test.com", "na2");
    controller.addPerson("Test3", "Tester3", "5432109876", "test3@test.com", "na3");
  }

  @Test
  void testAddPerson() {
    // Add person by providing the information only (we are not creating the Person object)
    assertEquals(3, controller.personSize)
    assertTrue(controller.addPerson("Test4", "Tester4",
      "0987612345", "test4@test.com", "na4"))
    assertEquals(4, controller.personSize)

    // Verify the last person has the information we added
    assertEquals("Test4", controller.getLastPerson().firstName)
    assertEquals("Tester4", controller.getLastPerson().lastName)
    assertEquals("0987612345", controller.getLastPerson().phone)
    assertEquals("test4@test.com", controller.getLastPerson().email)
    assertEquals("na4", controller.getLastPerson().address)

    // Create Person object and add to the collection
    assertEquals(4, controller.personSize)
    Person newPerson = new Person("Test5", "Tester5", "0987612355", "test5@test.com", "na5")
    assertTrue(controller.addPerson(newPerson))
    assertEquals(5, controller.personSize)
    assertSame(newPerson, controller.getLastPerson())

    // Verify the last person has the information we added
    assertEquals(newPerson.firstName, controller.getLastPerson().firstName)
    assertEquals(newPerson.lastName, controller.getLastPerson().lastName)
    assertEquals(newPerson.phone, controller.getLastPerson().phone)
    assertEquals(newPerson.email, controller.getLastPerson().email)
    assertEquals(newPerson.address, controller.getLastPerson().address)

    // Verify we can't add null people or null information
    assertEquals(5, controller.personSize)
    assertFalse(controller.addPerson(null, null, null, null, null))
    assertFalse(controller.addPerson("Test", null, null, null, null))
    assertFalse(controller.addPerson("Test", "Tester", null, null, null))
    assertFalse(controller.addPerson("Test", "Tester", "1234567890", null, null))
    assertFalse(controller.addPerson("Test", "Tester", "1234567890", "test@tester.com", null))
    assertFalse(controller.addPerson(null))
    assertEquals(5, controller.personSize)

    // Verify we can't add empty information
    assertFalse(controller.addPerson("", "", "", "",""))

    assertEquals(5, controller.personSize)
  }

  @Test
  void testRemovePerson() {
    // Create person object and add it to collection so we can later remove it
    assertEquals(3, controller.personSize)
    Person newPerson = new Person("Test4", "Tester4", "0987612344", "test4@test.com", "na4")
    assertTrue(controller.addPerson(newPerson))
    assertEquals(4, controller.personSize)
    assertSame(newPerson, controller.getLastPerson())

    // Verify we can remove the person
    assertTrue(controller.removePerson(newPerson))
    assertEquals(3, controller.personSize)
    assertNotSame(newPerson, controller.getLastPerson())

    // Remove existing person object though id (index)
    Person oldPerson = controller.getPerson(1)
    assertTrue(controller.removePerson(1))
    assertEquals(2, controller.personSize)
    assertNotSame(oldPerson, controller.getPerson(1))

    // Can't remove negative id person object
    assertFalse(controller.removePerson(-1))

    // Can't remove id which is greater than size
    assertFalse(controller.removePerson(5))

    // Can't remove null object
    assertFalse(controller.removePerson(null))
  }

  @Test
  void testGetPerson() {
    // Get existing person at given id (index) and verify it's information
    Person p = controller.getPerson(1)
    assertEquals("Test2", p.firstName)
    assertEquals("Tester2", p.lastName)
    assertEquals("0987654321", p.phone)
    assertEquals("test2@test.com", p.email)
    assertEquals("na2", p.address)
  }

  @Test
  void testGetPersonSize() {
    // Add and remove person object to check if size of collection changes
    assertEquals(3, controller.personSize)
    assertTrue(controller.addPerson("t","tt", "1230981230", "t@t.t", "none"))
    assertEquals(4, controller.personSize)
    assertTrue(controller.removePerson(3))
    assertTrue(controller.removePerson(2))
    assertEquals(2, controller.personSize)

    assertTrue(controller.removePerson(controller.getLastPerson()))
    assertEquals(1, controller.personSize)

    assertTrue(controller.removePerson(controller.getLastPerson()))
    assertEquals(0, controller.personSize)

    assertFalse(controller.removePerson(controller.getLastPerson()))
    assertEquals(0, controller.personSize)
  }

  @Test
  void testUpdatePerson() {
    // Update first name
    assertEquals("Test1", controller.getFirstPerson().firstName)
    assertTrue(controller.updatePerson(0, "Test11", null, null, null, null))
    assertEquals("Test11", controller.getFirstPerson().firstName)

    // Update last name
    assertEquals("Tester1", controller.getFirstPerson().lastName)
    assertTrue(controller.updatePerson(0, null, "Tester11", null, null, null))
    assertEquals("Tester11", controller.getFirstPerson().lastName)

    // Update phone number
    assertEquals("1234567890", controller.getFirstPerson().phone)
    assertTrue(controller.updatePerson(0, null, null, "0987654321", null, null))
    assertEquals("0987654321", controller.getFirstPerson().phone)

    // Update email address
    assertEquals("test1@test.com", controller.getFirstPerson().email)
    assertTrue(controller.updatePerson(0, null, null, null, "test11@test.com", null))
    assertEquals("test11@test.com", controller.getFirstPerson().email)

    // Update address
    assertEquals("na1", controller.getFirstPerson().address)
    assertTrue(controller.updatePerson(0, null, null, null, null, "na11"))
    assertEquals("na11", controller.getFirstPerson().address)
  }

  @Test
  void testToString() {
    String expected = "0: Test1 Tester1\n" +
      "1: Test2 Tester2\n" +
      "2: Test3 Tester3\n"
    assertEquals(expected, controller.toString())
  }
}

