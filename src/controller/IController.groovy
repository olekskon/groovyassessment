/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 ***************************************************************/
/**
 *
 * User: Oleksandr Kononov
 * Date: 2019-08-09
 */

package controller

import model.Person

interface IController {

  /**
   * Adds given person object to the address book collection.
   * @param person to be added
   * @return boolean success status of operation
   */
  boolean addPerson(Person person)

  /**
   * Creates and adds a person object using the given information to the address book collection.
   * @param firstName persons first name
   * @param lastName persons last name
   * @param phone persons phone number
   * @param email persons email address
   * @param address persons address
   * @return boolean success status of operation
   */
  boolean addPerson(String firstName, String lastName, String phone, String email, String address)

  /**
   * Removes given person from the address book collection using their id in the collection.
   * @param id of the person to be removed
   * @return boolean success status of operation
   */
  boolean removePerson(int id)

  /**
   * Removes given person object from the address book collection.
   * @param person object to be removed
   * @return boolean success status of operation
   */
  boolean removePerson(Person person)

  /**
   * @return address book collection
   */
  List<Person> getPersons()

  /**
   * @param id of the person to be returned from the address book collection
   * @return person object from the address book
   */
  Person getPerson(int id)

  /**
   * Get the last added person from the address book collection.
   * @return person object
   */
  Person getLastPerson()

  /**
   * Get the first added person from the address book collection.
   * @return person object
   */
  Person getFirstPerson()

  /**
   * Get the size of the address book collection.
   * @return int value
   */
  int getPersonSize()

  /**
   * Using given parameters update the information of the person by id from the address book collection.
   * @param id of the person to be updated
   * @param firstName String first name
   * @param lastName String last name
   * @param phone String phone number
   * @param email String email addresss
   * @param address String address
   * @return boolean success status of operation
   */
  boolean updatePerson(int id, String firstName, String lastName, String phone, String email, String address)

}