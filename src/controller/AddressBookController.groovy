/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 ***************************************************************/
/**
 *
 * User: Oleksandr Kononov
 * Date: 2019-08-09
 */

package controller

import model.Person

class AddressBookController implements IController {

  private List<Person> personList;

  AddressBookController(){
    personList = new ArrayList<>()
  }

  /**
   * Private setter for personList to prevent setting personList
   * @param p
   */
  private void setPersonList(p){}

  /**
   * Adds person object to personList collection.
   * @param person to be added
   * @return boolean status of success
   */
  @Override
  boolean addPerson(Person person) {
    if (!person?.nullReport()?.isBlank()) return false
    return personList.add(person)
  }

  /**
   * Creates and adds person object to the personList collection.
   * @param firstName persons first name
   * @param lastName persons last name
   * @param phone persons phone number
   * @param email persons email address
   * @param address persons address
   * @return boolean status of success
   */
  @Override
  boolean addPerson(String firstName, String lastName, String phone, String email, String address) {
    return addPerson(new Person(firstName, lastName, phone, email, address))
  }

  /**
   * Removes a person from the personList collection using their index.
   * @param id of the person to be removed
   * @return boolean status of success
   */
  @Override
  boolean removePerson(int id) {
    if (id < 0 || id >= personList.size()) return false
    return removePerson(personList[id])
  }

  /**
   * Removes a person from the personList collection.
   * @param person object to be removed
   * @return boolean status of success
   */
  @Override
  boolean removePerson(Person person) {
    if (person != null) return personList.remove(person)
    return false
  }

  /**
   * Get personList collection
   * @return list of person objects
   */
  @Override
  List<Person> getPersons() {
    return personList
  }

  /**
   * Gets person object from personList collection using index.
   * @param id of the person to be returned from the address book collection
   * @return person object
   */
  @Override
  Person getPerson(int id) {
    if (!personList) return null
    if (id < 0 || id >= personList.size()) return null
    return personList[id]
  }

  /**
   * Gets last person added to the personList.
   * @return person object
   */
  @Override
  Person getLastPerson() {
    if (!personList) return null
    return personList[-1]
  }

  /**
   * Get first person added to the personList.
   * @return
   */
  @Override
  Person getFirstPerson() {
    if (!personList) return null
    return personList.head()
  }

  /**
   * Get size of the personList collection.
   * @return
   */
  @Override
  int getPersonSize() {
    return personList.size()
  }

  /**
   * Updates information of a person object from the peronsList using index.
   * @param id of the person to be updated
   * @param firstName String first name
   * @param lastName String last name
   * @param phone String phone number
   * @param email String email addresss
   * @param address String address
   * @return boolean success status
   */
  @Override
  boolean updatePerson(int id, String firstName, String lastName, String phone, String email, String address) {
    if (id < 0 || id >= personList.size()) return false
    Person personToEdit = personList[id]
    String currentData = personToEdit.toString()
    personToEdit.firstName = firstName
    personToEdit.lastName = lastName
    personToEdit.phone = phone
    personToEdit.email = email
    personToEdit.address = address
    return personToEdit.toString() != currentData
  }

  /**
   * Prints all person objects first and last names with corresponding index of the personList collection.
   * @return formatted string
   */
  @Override
  String toString(){
    StringBuilder result = new StringBuilder()
    personList.eachWithIndex { Person p, int i -> result.append "$i: $p.firstName $p.lastName\n"}
    return result.toString()
  }
}
