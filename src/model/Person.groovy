/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 ***************************************************************/
/**
 *
 * User: Oleksandr Kononov
 * Date: 2019-08-09
 */

package model

import groovy.transform.ToString

@ToString(includeNames = true)
class Person {
  String firstName, lastName, phone, email, address;

  Person(String firstName, String lastName, String phone, String email, String address){
    setFirstName(firstName)
    setLastName(lastName)
    setPhone(phone)
    setEmail(email)
    setAddress(address)
  }

  void setFirstName(String firstName){
    if (firstName == null) return
    if (firstName ==~ /\w+/) this.firstName = firstName
  }

  void setLastName(String lastName){
    if (lastName == null) return
    if (lastName ==~ /\w+/) this.lastName = lastName
  }

  void setPhone(String phone){
    if (phone == null) return
    if (phone ==~ /\+?\d{10,}/) this.phone = phone
  }

  void setEmail(String email){
    if (email == null) return
    if (email ==~ /\w+@\w+\.\w+/) this.email = email
  }

  void setAddress(String address){
    if (address == null) return
    if (address =~ /\w+/) this.address = address
  }

  /**
   * Checks if any of the fields are null and reports them.
   * @return String report of unset fields
   */
  String nullReport(){
    String report = ""
    this.properties.each { if (!it.value) report += "$it.key is not set!\n" }
    report
  }
}
