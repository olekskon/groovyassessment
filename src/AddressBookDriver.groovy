import controller.AddressBookController
import controller.IController
import model.Person
import view.ConsoleView
import view.IView
import view.Menu

/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 ***************************************************************/
/**
 *
 * User: Oleksandr Kononov
 * Date: 2019-08-09
 */


class AddressBookDriver {

  IController controller
  IView view
  boolean quit

  AddressBookDriver(){
    quit = false
    controller = new AddressBookController()
    view = new ConsoleView()

    // Populate the menu operations for different menu screens
    view.addMenuOperation(Menu.MAIN, this.&mainMenu)
    view.addMenuOperation(Menu.ADD, this.&addMenu)
    view.addMenuOperation(Menu.REMOVE, this.&removeMenu)
    view.addMenuOperation(Menu.EDIT, this.&editMenu)
    view.addMenuOperation(Menu.LIST_USER, this.&listUserMenu)
    view.addMenuOperation(Menu.LIST_USERS, this.&listAllUsersMenu)
    view.addMenuOperation(Menu.QUIT, {quit = true})

    controller.addPerson("Mr", "Bean", "1234567890", "mrbean@bean.com", "England")
    controller.addPerson("Rob", "OConnor", "1234509876", "rob@oconnor.ie", "Ireland")
    controller.addPerson("Kieran", "Murphy", "0987654321", "kieran@murphy.ie", "Ireland")
    controller.addPerson("John", "Kelly", "0123456789", "john@keylly.ie", "Ireland")
  }

  List promptAndGatherPersonData(){
    def info = []
    Person.declaredFields
      .findAll{!it.synthetic}
      .each{
        String response = view.prompt("Please enter users ${it.name}: ")
        info.add(response)
      }
    info
  }

  def mainMenu(){
    switch(view.readInput()){
      case ConsoleView.ADD:
        view.changeMenu(Menu.ADD);
        break;
      case ConsoleView.REMOVE:
        view.changeMenu(Menu.REMOVE);
        break;
      case ConsoleView.EDIT:
        view.changeMenu(Menu.EDIT);
        break;
      case ConsoleView.LIST_USER:
        view.changeMenu(Menu.LIST_USER);
        break;
      case ConsoleView.LIST_USERS:
        view.changeMenu(Menu.LIST_USERS);
        break;
      case ConsoleView.QUIT:
        quit = true;
        break;
      default:
        view.show("Unexpected input, please follow the menu!\n");
        break;
    }
  }

  def addMenu(){
    List<String> info = promptAndGatherPersonData()
    Person newUser = new Person(*info)

    if (!controller.addPerson(newUser)) view.show("\nERROR\n${newUser.nullReport()}")
    else view.show("New User added successfully!\n")
  }

  def removeMenu(){
    if (!controller.personSize){
      view.show("No users to remove!")
      view.changeMenu(Menu.MAIN)
      return
    }

    view.show(controller)
    int index = view.prompt("Select user to remove: ") as int

    if (!controller.removePerson(index)) view.show("Failed to remove user!\n")
    else view.show("Removed user successfully!\n")
  }

  def editMenu(){
    if (!controller.personSize){
      view.show("No users to edit!")
      view.changeMenu(Menu.MAIN)
      return
    }

    view.show(controller)
    int index = view.prompt("Select user to edit: ") as int
    if (!controller.getPerson(index)){
      view.show("Wrong index selected!\n")
      return
    }

    view.show("Enter edit data (leave blank to ignore)\n")

    List<String> info = promptAndGatherPersonData()

    if (!controller.updatePerson(index, *info)) view.show("Could not update person!\n")
    else view.show("Updated person successfully!\n")
  }

  def listUserMenu(){
    if (!controller.personSize){
      view.show("No users to list!")
      view.changeMenu(Menu.MAIN)
      return
    }

    view.show(controller)
    int index = view.prompt("Select user to view: ") as int

    Person user = controller.getPerson(index)
    view.show(user ? user : "Wrong index selected!\n")
  }

  def listAllUsersMenu(){
     if (!controller.personSize){
       view.show("No users to list!")
       view.changeMenu(Menu.MAIN)
       return
     }

    Closure sortType

    switch (view.readInput()){
      case ConsoleView.SORT_BY_FIRST:
        sortType = {p1, p2 -> p1.firstName <=> p2.firstName}
        break
      case ConsoleView.SORT_BY_LAST:
        sortType = {p1, p2 -> p1.lastName <=> p2.lastName}
        break
      default:
        view.show("Unexpected input, please follow the menu!\n")
        return
    }

    controller.getPersons().sort(sortType).each{view.show"$it.firstName $it.lastName"}
  }

  void run(){
    while(!quit) view.display()
  }

  static void main(String[] args){
    new AddressBookDriver().run()
  }
}
