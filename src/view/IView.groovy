/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 ***************************************************************/
/**
 *
 * User: Oleksandr Kononov
 * Date: 2019-08-09
 */

package view

interface IView {

  /**
   * Displays the current menu to user and triggers the current menu state operation.
   */
  void display()

  /**
   * Show data to user.
   * @param data
   */
  void show(def data)

  /**
   * Add menu event handling through runnable operations.
   * @param forMenu Menu enum state
   * @param operation Runnable operation to execute for given menu state
   */
  void addMenuOperation(Menu state, Closure operation)

  /**
   * Change current menu state to given menu.
   * @param menu Menu enum state
   */
  void changeMenu(Menu state)

  /**
   * Directly read input from user.
   * @return users input
   */
  def readInput()

  /**
   * Prompt user for input with a message.
   * @param message prompt
   * @return user input
   */
  def prompt(String message)
}