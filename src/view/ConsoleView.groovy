/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 ***************************************************************/
/**
 *
 * User: Oleksandr Kononov
 * Date: 2019-08-09
 */

package view

class ConsoleView implements IView{

  /* Main Menu Options */
  static final public String ADD = "a"
  static final public String REMOVE = "r"
  static final public String EDIT = "e"
  static final public String LIST_USER = "l"
  static final public String LIST_USERS = "s"
  static final public String QUIT = "q"

  /* List Users Menu Options */
  static final public String SORT_BY_FIRST = "f"
  static final public String SORT_BY_LAST = "l"

  HashMap<Menu, Closure> menuOperations
  Menu currentMenu

  /**
   * Prevent setter being created for menuOperations.
   * @param m
   */
  private void setMenuOperations(m) {}

  ConsoleView(){
    currentMenu = Menu.MAIN
    menuOperations = new HashMap<>()
  }

  /**
   * Depending on currentMenu state, display corresponding menu in the view (console)
   * and execute the currentMenu state operation if available.
   */
  @Override
  void display(){
    String menuName = currentMenu.name().replaceAll('_',' ')
    switch (currentMenu){
      case (Menu.ADD..Menu.LIST_USER):
        println "\n$menuName MENU\n\n"
        break
      case Menu.LIST_USERS:
        print "\n$menuName MENU\n\n" +
          "$SORT_BY_FIRST. Sort all users by first name\n" +
          "$SORT_BY_LAST. Sort all users by last name\n"+
          "> "
        break
      case Menu.MAIN:
        print "\n$menuName MENU\n\n" +
          "$ADD. Add new user\n" +
          "$REMOVE. Remove existing user\n" +
          "$EDIT. Edit existing user\n" +
          "$LIST_USER. List specific user\n" +
          "$LIST_USERS. List all users\n" +
          "$QUIT. Quit\n" +
          "> "
    }

    Menu preChangeMenu = currentMenu;
    menuOperations[currentMenu]?.call()

    // Prompt user to return to main menu from current (non main) menu after operation is complete
    if (preChangeMenu != Menu.MAIN) {
      def response = prompt("Go back to main menu? (y/n): ")
      if (response in ['y', 'Y']) currentMenu = Menu.MAIN
    }
  }

  @Override
  void show(data) {
    if (data) println data
  }

  /**
   * Add an operation association with a given menu state.
   * @param forMenu state
   * @param operation closure operation
   */
  @Override
  void addMenuOperation(Menu state, Closure operation) {
    menuOperations[state] = operation
  }

  /**
   * Change current menu state.
   * @param state
   */
  @Override
  void changeMenu(Menu state) {
    currentMenu = state
  }

  /**
   * Read user input from stdin.
   * @return user input
   */
  @Override
  def readInput() {
    System.in.newReader().readLine()
  }

  /**
   * Prompt user for input with message.
   * @param message String for prompt
   * @return
   */
  @Override
  def prompt(String message) {
    print message
    System.in.newReader().readLine()
  }
}
