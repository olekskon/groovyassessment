/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 ***************************************************************/
/**
 *
 * User: Oleksandr Kononov
 * Date: 2019-08-09
 */

/*
 * Question 4.
 * How are implicit getters and setters created in Groovy?
 * Answer variable: value
 *
 * Question 5.
 * How do you stop a setter from being created?
 * Answer variables(s): otherValue, notSetterValue
 */

class GetterSetterQuestion{
  def value
  final def otherValue
  def notSetterValue

  GetterSetterQuestion(v1, v2, v3){
    value = v1
    otherValue = v2
    notSetterValue = v3
  }

  private void setNotSetterValue(def other){}
}

def getterSetter = new GetterSetterQuestion(10,  20,  30)

// Getters and setters for value
assert getterSetter.value == 10
getterSetter.value = 15
assert getterSetter.value == 15

assert getterSetter.getValue() == 15
getterSetter.setValue(10)
assert getterSetter.getValue() == 10

try{
  getterSetter.otherValue = 25
  assert false
} catch(ReadOnlyPropertyException e){
  assert getterSetter.otherValue == 20
}

getterSetter.notSetterValue = 50
assert getterSetter.notSetterValue == 30


/*
 * Question 8.
 * How do I create a Map in Groovy
 */
Map m1 = [a:1]
Map m2 = [:]
Map m3 = new HashMap()

assert m1.getClass().name == 'java.util.LinkedHashMap'
assert m2.getClass().name == 'java.util.LinkedHashMap'
assert m3.getClass().name == 'java.util.HashMap'

/*
 * Question 9.
 * How do I create a List in Groovy
 */
def l1 = [1,2,3]
def l2 = []
def l3 = (1..10).toList()
def l4 = new ArrayList()

assert l1.getClass().name == 'java.util.ArrayList'
assert l2.getClass().name == 'java.util.ArrayList'
assert l3.getClass().name == 'java.util.ArrayList'
assert l4.getClass().name == 'java.util.ArrayList'

/*
 * Question 10.
 * How do I create a Set in Groovy?
 */
def s1 = [1,2,3] as Set
Set s2 = []
def s3 = new HashSet()

assert s1.getClass().name == 'java.util.LinkedHashSet'
assert s2.getClass().name == 'java.util.LinkedHashSet'
assert s3.getClass().name == 'java.util.HashSet'

/*
 * Question 13.
 * Demonstrate usage of the ".each" method on collections
 */
m1.each {println it.key + it.value}
l1.each {println it}
s1.each {println it}

/*
 * Question 14.
 * Use the ".find" and ".findAll" methods on collections to find objects by property values
 */
assert l1.find {it >= 2} == 2
assert l1.findAll {it >= 2} == [2,3]

/*
 * Question 15.
 * Write a simple Groovy application that will print the names of all files in a directory
 */
print "Enter Directory To Read: "
String directoryToRead = System.in.newReader().readLine().replaceFirst("^~", System.getProperty("user.home"))
new File(directoryToRead)?.eachFile {println it.name}

/*
 * Question 16.
 * Write a simple Groovy application that will print the names of all files in a directory
 * and subdirectories
 */
print "Enter Directory To Read Recursively: "
directoryToRead = System.in.newReader().readLine().replaceFirst("^~", System.getProperty("user.home"))
new File(directoryToRead)?.eachFileRecurse {println it.name}
