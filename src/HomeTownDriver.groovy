import groovy.xml.MarkupBuilder

/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 ***************************************************************/
/**
 *
 * User: Oleksandr Kononov
 * Date: 2019-08-09
 */

/*
 * Question 17.
 * Using the Groovy MarkupBuilder, write a simple Groovy application that creates a one
 * page HTML file with 1-2 text paragraphs about your hometown, as well as 2-3 images,
 * and a number of link to local restaurant websites.
 */

def writer = new StringWriter()
def html = new MarkupBuilder(writer)
html.html {
  head {
    title: "Sumy, Ukraine"
  }
  body{
    h1 "Sumy Ukraine"
    img (src: "https://destinations.com.ua/storage/crop/articles/avatar_900_max.jpg", height: "400", width: "800")
    img (src: "https://ukrainetrek.com/images/sumy-ukraine-city-views-4.jpg", height: "400", width: "800")
    p "Sumy is a city in the north-east part of Ukraine and is the captial of the Sumy Oblast(region)."
    p "It's temperatures during summer can reach to 40+ degrees celsius and go as low as -30 during winter seasons."
    p "It's notable for it's river Psel which is very popular during summer time."
    h3 "Some local restaurant websites:"
    ul{
      li { a href: "https://www.sushiya.ua/en/restorany/restorany_restorany/sumy/", "Sushiya" }
      li { a href: "https://www.tripadvisor.ie/Restaurants-g2693141-Sumy_Oblast.html", "TripAdvisor" }
      li { a href: "https://www.thingstodopost.com/top-10-restaurants-in-sumy-ukraine-115614", "ThingsToDoPost" }
    }
  }
}

print "Enter Directory To Create HTML In: "
String dir = System.in.newReader().readLine().replaceFirst("^~", System.getProperty("user.home"))
new File(dir+"index.html")?.write(writer.toString())
