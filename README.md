# Groovy Assessment - Oleksandr Kononov

## Task One
Answer the questions from the assessment document and create a Groovy file to demonstrate/answer questions marked with the asterisk symbol (*).

The answers document for **Task One** will be on the Google Drive in the `Training-Assessments/Groovy` folder.

## Task Two
'Convert' Java assessment application, Address Book, to using Groovy code where possible.
The aim should be to:
- Create an application that shows off the methods added to the JDK to make it more “Groovy” by reducing existing Java boilerplate code.
- Replace all for loops and iterations with .each, .find or findAll.
- Demonstrate the usage of useful methods added to classes and interfaces in the Groovy GDK:
    - File (not needed in this case).
    - Collections
    - String
    - etc.
- Question the remaining Java code in your converted application. Can it be replaced with Groovy code?

The project should be created and run through IntelliJ IDEA, with command being entered through IntelliJ console.

### Running Address Book
- Navigate to `Groovy_Assessment/src/` directory.
- Run `AddressBookDriver.groovy` file.
- A menu will be shown in the IntelliJ IDEA console which signifies that the application is running.

#### Usage
When running the **Address Book** application, the user is first presented with the _Main Menu_ screen where options are displayed in the format:

`[LETTER]. [OPTION DESCRIPTION]`

##### Example
> a. Add new user
>
> r. Remove existing user
>
> e. Edit existing user
>
> l. List specific user
>
> s. List all users
>
> q. Quit
>
> \>

Input is taken by typing the `LETTER` character into the console.

##### Example
The following will enter the *Add new user submenu*
> \> a

### Running Unit Tests
- Navigate to `Groovy_Assessment/test/` directory.
- Open any of the test files which have the format `[CLASS NAME]Test.groovy`.
- Run the groovy file to run the test for that *CLASS NAME*.

